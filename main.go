package main

import "fmt"

const (
	waterForOneCup = 200
	milkForOneCup  = 50
	beansForOneCup = 15

	amountMessagePrefix = "Write how many "
	amountMessageSuffix = " the coffee machine has:"
	waterAmountMessage  = amountMessagePrefix + "ml of water" + amountMessageSuffix
	milkAmountMessage   = amountMessagePrefix + "ml of milk" + amountMessageSuffix
	beanAmountMessage   = amountMessagePrefix + "grams of coffee beans" + amountMessageSuffix
	cupAmountMessage    = amountMessagePrefix + "cups of coffee you will need:"

	waterForEspresso   = 250
	waterForLatte      = 350
	waterForCappuccino = 200
	milkForEspresso    = 0
	milkForLatte       = 75
	milkForCappuccino  = 100
	beansForEspresso   = 16
	beansForLatte      = 20
	beansForCappuccino = 12
	vanilla            = 20
	chocolate          = 10
	spice              = 5
	priceEspresso      = 4
	priceLatte         = 7
	priceCappuccino    = 6
	vanillaMarkup      = 1
	chocolateMarkup    = 2
	spiceMarkup        = 3
)

func main() {
	var waterAmountP = new(int)
	var milkAmountP = new(int)
	var beanAmountP = new(int)
	var vanillaAmountP = new(int)
	var chocolateAmountP = new(int)
	var spiceAmountP = new(int)
	var cupAmountP = new(int)
	var moneyAmountP = new(int)

	*waterAmountP = 400
	*milkAmountP = 540
	*beanAmountP = 120
	*vanillaAmountP = 100
	*chocolateAmountP = 50
	*spiceAmountP = 35
	*cupAmountP = 9
	*moneyAmountP = 550

	askAndProcessAction(waterAmountP, milkAmountP, beanAmountP, vanillaAmountP, chocolateAmountP, spiceAmountP, cupAmountP, moneyAmountP)
}

func makeCoffee() {
	fmt.Println("Starting to make a coffee")
	fmt.Println("Grinding coffee beans")
	fmt.Println("Boiling water")
	fmt.Println("Mixing boiled water with crushed coffee beans")
	fmt.Println("Pouring coffee into the cup")
	fmt.Println("Pouring some milk into the cup")
	fmt.Println("Coffee is ready!")
}

func printIngredientsAmounts(cupAmount int) {
	fmt.Println("For", cupAmount, "cups of coffee you will need:")
	fmt.Println(cupAmount*200, "ml of water")
	fmt.Println(cupAmount*50, "ml of milk")
	fmt.Println(cupAmount*15, "g of coffee beans")
}

func getNumberFromConsole(messageToPrint string) int {
	var number int
	fmt.Println(messageToPrint)
	fmt.Scan(&number)
	return number
}

func getPossibleAmountOfCups(waterInMachine int, milkInMachine int, beansInMachine int, cupsWanted int) int {
	possibleCupsForWater := waterInMachine / waterForOneCup
	possibleCupsForMilk := milkInMachine / milkForOneCup
	possibleCupsForBeans := beansInMachine / beansForOneCup

	return getMinOfThree(possibleCupsForWater, possibleCupsForMilk, possibleCupsForBeans)
}

func getMinOfThree(first, second, third int) int {

	if first <= second && first <= third {
		return first
	} else if second <= first && second <= third {
		return second
	} else {
		return third
	}
}

func printPossibility(possibleCupAmount int, wantedCupAmount int) {

	switch {
	case possibleCupAmount == wantedCupAmount:
		fmt.Println("Yes, I can make that amount of coffee")
	case possibleCupAmount < wantedCupAmount:
		fmt.Println("No, I can only make", possibleCupAmount, "cups of coffee")
	case possibleCupAmount > wantedCupAmount:
		fmt.Println("Yes, I can make that amount of coffee (and even", (possibleCupAmount - wantedCupAmount), "more than that)")
	}
}

func printAmountsInMachine(water *int, milk *int, beans *int, vanilla *int, chocolate *int, spice *int, cups *int, money *int) {
	fmt.Println("")
	fmt.Println("The coffee machine has:")
	fmt.Println(*water, "of water")
	fmt.Println(*milk, "of milk")
	fmt.Println(*beans, "of coffee beans")
	fmt.Println(*vanilla, "of vanilla")
	fmt.Println(*chocolate, "of chocolate")
	fmt.Println(*spice, "of spice")
	fmt.Println(*cups, "of disposable cups")
	fmt.Printf("$%d of money", *money)
	fmt.Println("")
}

func getAction() string {
	var action string
	fmt.Println("")
	fmt.Println("Write action (buy, fill, take, remaining, exit):")
	fmt.Scan(&action)
	return action
}

func askAndProcessAction(water *int, milk *int, beans *int, vanilla *int, chocolate *int, spice *int, cups *int, money *int) {

	var action string
	for action != "exit" {
		action = getAction()
		switch action {
		case "buy":
			handleBuy(water, milk, beans, vanilla, chocolate, spice, cups, money)
		case "fill":
			handleFill(water, milk, beans, vanilla, chocolate, spice, cups)
		case "take":
			handleTake(money)
		case "remaining":
			printAmountsInMachine(water, milk, beans, vanilla, chocolate, spice, cups, money)
		case "exit":
			return
		}
	}
}

func handleBuy(water *int, milk *int, beans *int, vanilla *int, chocolate *int, spice *int, cups *int, money *int) {
	var choice string
	var extra int
	fmt.Println("")
	fmt.Println("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino:")
	fmt.Scan(&choice)

	if choice == "back" {
		return
	}

	fmt.Println("Add 1 - vanilla, 2 - chocolate, 3 - spice, 4 - no extra ?")
	fmt.Scan(&extra)

	if checkAmounts(getChoiceAsInt(choice), water, milk, beans, vanilla, chocolate, spice, cups) {
		recalculateResources(getChoiceAsInt(choice), water, milk, beans, vanilla, chocolate, spice, extra, cups, money)
	}
}

func getChoiceAsInt(stringChoice string) int {

	var choice int
	switch stringChoice {
	case "1":
		choice = 1
	case "2":
		choice = 2
	case "3":
		choice = 3
	}

	return choice
}

func checkAmounts(choice int, waterP *int, milkP *int, beansP *int, vanillaP *int, chocolateP *int, spiceP *int, cupsP *int) bool {
	const messagePrefix string = "Sorry, not enough "
	waterNeeded := getAmountNeeded(choice, "water")
	milkNeeded := getAmountNeeded(choice, "milk")
	beansNeeded := getAmountNeeded(choice, "beans")
	vanillaNeeded := getAmountNeeded(choice, "vanilla")
	chocolateNeeded := getAmountNeeded(choice, "chocolate")
	spiceNeeded := getAmountNeeded(choice, "spice")
	switch {
	case *waterP < waterNeeded:
		fmt.Println(messagePrefix + "water!")
		return false
	case *milkP < milkNeeded:
		fmt.Println(messagePrefix + "milk!")
		return false
	case *beansP < beansNeeded:
		fmt.Println(messagePrefix + "beans!")
		return false
	case *vanillaP < vanillaNeeded:
		fmt.Println(messagePrefix + "vanilla!")
		return false
	case *chocolateP < chocolateNeeded:
		fmt.Println(messagePrefix + "chocolate!")
		return false
	case *spiceP < spiceNeeded:
		fmt.Println(messagePrefix + "spice!")
		return false
	case *cupsP < 1:
		fmt.Println(messagePrefix + "cups!")
		return false
	default:
		fmt.Println("I have enough resources, making you a coffee!")
		return true
	}
}

func getAmountNeeded(choice int, resource string) int {

	var amount int
	switch {
	case choice == 1 && resource == "water":
		amount = waterForEspresso
	case choice == 2 && resource == "water":
		amount = waterForLatte
	case choice == 3 && resource == "water":
		amount = waterForCappuccino
	case choice == 1 && resource == "milk":
		amount = milkForEspresso
	case choice == 2 && resource == "milk":
		amount = milkForLatte
	case choice == 3 && resource == "milk":
		amount = milkForCappuccino
	case choice == 1 && resource == "beans":
		amount = beansForEspresso
	case choice == 2 && resource == "beans":
		amount = beansForLatte
	case choice == 3 && resource == "beans":
		amount = beansForCappuccino
	case choice == 1 && resource == "money":
		amount = priceEspresso
	case choice == 2 && resource == "money":
		amount = priceLatte
	case choice == 3 && resource == "money":
		amount = priceCappuccino
	case resource == "vanilla":
		amount = vanilla
	case resource == "chocolate":
		amount = chocolate
	case resource == "spice":
		amount = spice
	}

	return amount
}

func recalculateResources(choice int, waterP *int, milkP *int, beansP *int, vanillaP *int, chocolateP *int, spiceP *int, extra int, cupsP *int, moneyP *int) {
	*waterP -= getAmountNeeded(choice, "water")
	*milkP -= getAmountNeeded(choice, "milk")
	*beansP -= getAmountNeeded(choice, "beans")
	*cupsP -= 1
	*moneyP += getAmountNeeded(choice, "money")

	switch extra {
	case 1: // vanilla
		*vanillaP -= getAmountNeeded(choice, "vanilla")
		*moneyP += vanillaMarkup
	case 2: // chocolate
		*chocolateP -= getAmountNeeded(choice, "chocolate")
		*moneyP += chocolateMarkup
	case 3: // spice
		*spiceP -= getAmountNeeded(choice, "spice")
		*moneyP += spiceMarkup
	}
}

func handleFill(water *int, milk *int, beans *int, vanilla *int, chocolate *int, spice *int, cups *int) {
	var waterToAdd int
	var milkToAdd int
	var beansToAdd int
	var vanillaToAdd int
	var chocolateToAdd int
	var spiceToAdd int
	var cupsToAdd int

	fmt.Println("")
	fmt.Println("Write how many ml of water you want to add:")
	fmt.Scan(&waterToAdd)
	fmt.Println("Write how many ml of milk you want to add:")
	fmt.Scan(&milkToAdd)
	fmt.Println("Write how many grams of coffee beans you want to add:")
	fmt.Scan(&beansToAdd)
	fmt.Println("Write how many grams of vanilla you want to add:")
	fmt.Scan(&vanillaToAdd)
	fmt.Println("Write how many grams of chocolate you want to add:")
	fmt.Scan(&chocolateToAdd)
	fmt.Println("Write how many grams of spice you want to add:")
	fmt.Scan(&spiceToAdd)
	fmt.Println("Write how many disposable coffee cups you want to add:")
	fmt.Scan(&cupsToAdd)

	*water += waterToAdd
	*milk += milkToAdd
	*beans += beansToAdd
	*vanilla += vanillaToAdd
	*chocolate += chocolateToAdd
	*spice += spiceToAdd
	*cups += cupsToAdd
}

func handleTake(money *int) {
	fmt.Println("")
	fmt.Printf("I gave you $%d", *money)
	fmt.Println("")
	*money -= *money
}
